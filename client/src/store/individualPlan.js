import IndividualPlan from '../services/IndividualPlan.js'
import HelperFunction from "../services/HelperFunction.js"
export default {
    state: {
        individualPlan: []
    },
    mutations: {
        set_individualPlan(state, payload){
            state.individualPlan = payload
        },
        remove_Task(state, payload){
            state.individualPlan.tasks = HelperFunction.remove_item(state.individualPlan.tasks, payload)
        },
        update_Task(state, payload){
            state.individualPlan = payload
        },
        update_individualPlan(state,payload){
            state.individualPlan[payload.field]= payload.value
        },
        reset(state){
            state.individualPlan = []
        }
    },
    actions: {
        resetIndividualPlan({commit}){
            commit("reset")
        },
        async addToIndividualPlan({commit}, payload){
            let response = await IndividualPlan.add(this._vm.$session.get('token'), payload)
            console.log(response);
            commit("set_individualPlan", response.data)
        },
        async getIndividualPlan({commit}){
            let response = await IndividualPlan.get(this._vm.$session.get('token'))
            commit('set_individualPlan', response.data)
        },
        async deleteTask({commit}, payload){
            let response = await IndividualPlan.delete_task(this._vm.$session.get('token'), payload)
            console.log(response);
            commit("remove_Task", payload)
            console.log(payload)
        },
        async updateTask({commit}, payload){
            let response = await IndividualPlan.update_task(this._vm.$session.get('token'), payload)
            console.log(payload.body);
            commit("update_Task", response.data)
        },
        async updatePlan({commit}, payload){
            let response = await IndividualPlan.update_plan(this._vm.$session.get('token'), payload)
            console.log(response);
            commit("update_individualPlan", {field:"on_check",value:response.data.on_check})
        },
        async deletePlan({commit}){
            let response = await IndividualPlan.delete_plan(this._vm.$session.get('token'))
            console.log(response);
            commit("reset")
        },
        async updateTable({commit},payload){
            // let table_arr = []
            // table_arr.push(payload)
            let response = await IndividualPlan.update_table(this._vm.$session.get('token'), {table: payload})
            console.log(response)
        }
    },
    getters: {
        getIndividualPlan: state => state.individualPlan,
    }
}