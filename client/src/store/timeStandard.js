import TimeStandard from "../services/TimeStandard.js"
import HelperFunction from "../services/HelperFunction.js"
export default {
    state: {
        timeStandard: []
    },
    mutations: {
        set_timeStandard(state, payload){
            state.timeStandard = payload
        },
        push_timeStandard(state, payload){
            state.timeStandard.push(payload)
        },
        remove_timeStandard(state, payload){
            state.timeStandard = HelperFunction.remove_item(state.timeStandard, payload.id)
            console.log(payload)
        },
        update_timeStandard(state, payload){
            state.timeStandard.splice(state.timeStandard.findIndex(el=>el._id===payload._id), 1, payload)
        }
    },
    actions: {
        async fetchTimeStandard({commit}){
            let response = await TimeStandard.getTimeStandard(this._vm.$session.get('token'))
            commit("set_timeStandard", response.data)
            console.log(response);
        },
        async deleteTimeStandard({commit}, payload){
            let response = await TimeStandard.deleteTimeStandard(this._vm.$session.get('token'), payload)
            // console.log(payload)
            commit("remove_timeStandard", payload)
            // console.log(response);
        },
        async editTimeStandard({commit}, payload){
            let response = await TimeStandard.editTimeStandard(this._vm.$session.get('token'), payload)
            console.log(response);
            commit("update_timeStandard", response.data)
        },
        async addTimeStandard({commit}, payload){
            let response = await TimeStandard.addTimeStandard(this._vm.$session.get('token'), payload)
            console.log(response);
            commit("push_timeStandard", response.data)
        }
    },
    getters: {
        getTimeStandard: state => state.timeStandard 
    }
}