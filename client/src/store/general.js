export default {
    state: {
        sectionList: [{
                title: "Навчальна робота",
                value: "educational"
            },
            {
                title: "Науково-дослідницька й творча робота",
                value: "scientific"
            },
            {
                title: "Організаційно-методична робота",
                value: "organizational"
            },
            {
                title: "Навчально-методична робота",
                value: "educational-methodical"
            },
            {
                title: "Виховна й громадська робота",
                value: "educational-public"
            },
        ],
        roleList: [{
                title: "Викладач",
                value: "teacher"
            },
            {
                title: "Перевіряючий",
                value: "checker"
            },
            {
                title: "Затверджуючий",
                value: "approver"
            },
            {
                title: "Адміністратор ресурсу",
                value: "admin"
            },
        ],
        cathedraList: [{
            title: "Комп'ютерних та інформаційних технологій і систем",
            value: 'kitis'
        },
        {
            title: "Kомп'ютерної інженерії",
            value: 'ki'
        }]
    },
    mutations: {

    },
    actions: {

    },
    getters: {
        getRoleList: state => state.roleList,
        getSectionList: state => state.sectionList,
        getCathedraList: state => state.cathedraList


    }
}