import Auth from "../services/Auth.js"
import Admin from "../services/Admin.js";
export default {
    state: {
        token: null,
        user: {},
        error: []
    },
    mutations: {
        set_token(state, payload) {
            state.token = payload
        },
        unset_token(state) {
            state.token = null
        },
        set_user(state, payload) {
            state.user = payload
        },
        set_error(state, payload) {
            state.error.push(payload)
        },
        unset_error(state) {
            state.error = []
        }
    },
    actions: {
        async login({
            commit
        }, payload) {
            let res = await Auth.login({
                "email": payload.email,
                "password": payload.password
            })
            console.log(res)
            commit("set_error", res.data.error)
            commit("set_token", res.data.token)
            this._vm.$session.set('token', res.data.token)
        },
        get_session({
            commit
        }) {
            if (this._vm.$session.has('token')) {
                commit("set_token", this._vm.$session.get('token'))
            }
        },
        logout({
            commit
        }) {
            commit("unset_token")
            commit("unset_error")
            this._vm.$session.destroy()
        },
        async fetchProfile({
            commit
        }) {
            let response = await Auth.getProfile(this._vm.$session.get("token"))
            commit('set_user', response.data)
        },
        async add_user({
            commit
        }, payload) {
            let response = await Admin.addNewUser(this._vm.$session.get("token"), payload)
            console.log(response);
        },
        async update_profile({
            commit
        }, payload) {
            let response = await Auth.update(this._vm.$session.get('token'), payload)
            console.log(response);
        },
        async delete_account({
            commit
        }) {
            let response = await Auth.delete_account(this._vm.$session.get('token'))
            console.log(response);
        },
        async update_role({
            commit
        }, payload) {
            let response = await Admin.updateRole(this._vm.$session.get("token"), payload)
            console.log(response)
        },
        async delete_user({
            commit
        }, payload) {
            let response = await Admin.deleteUser(this._vm.$session.get("token"), payload)
            console.log(response)
        },
        async change_password({commit}, payload){
            let response = await Auth.changePassword(this._vm.$session.get("token"), payload)
            console.log(response)
        }
    },
    getters: {
        getToken: state => state.token,
        getUser: state => state.user,
        getError: state => state.error,
    },
};