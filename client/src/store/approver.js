import Approver from '../services/Approver.js'
export default {
    state: {
        approvedList:[],
    },
    mutations: {
        setApprovedList(state, payload){
            state.approvedList = payload
        },
        reset(state){
            state.approvedList = []
        }
    },
    actions: {
        resetApprovedList({commit}){
            commit("reset")
        },
       async get_approver({commit}){
        let response = await Approver.get_approver(this._vm.$session.get('token')) 
        console.log(response)
        commit('setApprovedList', response.data)
       },
       async set_approved({commit}, payload){
           let response = await Approver.set_approved(this._vm.$session.get('token'), payload)
           console.log(response)
       }
    },
    getters: {
        getApprovedList: state => state.approvedList
    }
}
