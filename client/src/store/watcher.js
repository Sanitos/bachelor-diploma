import Watcher from '../services/Watcher.js'
export default {
    state: {
        plans:[],
    },
    mutations: {
        set_plan(state,payload){
            state.plans = payload
        },
        reset(state){
            state.plans = []
        }
    },
    actions: {
        resetPlans({commit}){
            commit("reset")
        },
        async get_plans({commit}){
            let response =  await Watcher.get_plan(this._vm.$session.get('token'))
            let plan_on_check = []
            let plan_checked = []
            commit('set_plan', response.data)
            console.log(response);
        },
        async check_plan({commit}, payload){
            let response = await Watcher.check_task(this._vm.$session.get('token'), payload)
            console.log(response)
        },
        async send_remark({commit}, payload){
            console.log(payload)
        }
    },
    getters: {
        getPlan: state => state.plans
    }
}
