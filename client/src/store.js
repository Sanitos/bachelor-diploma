import Vue from 'vue'
import Vuex from 'vuex'
import userModule from "./store/user.js"
import timeStandardModule from "./store/timeStandard.js"
import individualPlanModule from './store/individualPlan.js'
import watcherModule from './store/watcher.js'
import approverModule from './store/approver.js'
import generalModule from './store/general.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules:{
    userModule,
    timeStandardModule,
    individualPlanModule,
    watcherModule,
    approverModule,
    generalModule
  }
})
