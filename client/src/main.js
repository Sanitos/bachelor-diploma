import Vue from 'vue'
import './plugins/vuetify'
import './plugins/vue-session'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
var vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
vm.$session.start()
vm.$store.dispatch("get_session")
