import axios from "axios";

export default (token = null) => {
    var axios_params = {}
    if (token != null) {
        axios_params = {
            baseURL: process.env.VUE_APP_API_URL,

            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Token ${token}`
            }
        }
    } else {
        axios_params = {
            baseURL: process.env.VUE_APP_API_URL,

        }

    }
    return axios.create(axios_params)
}