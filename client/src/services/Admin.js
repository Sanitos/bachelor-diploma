import Api from './Api.js'
import querystring from 'querystring'
const urlPrefix = "/admin/"
export default{
    getUserList(token){
        return Api(token).get(`${urlPrefix}userlist`)
    },
    addNewUser(token, params){
        return Api(token).post(`${urlPrefix}signup`,querystring.stringify(params))
    },
    updateRole(token, params){
        return Api(token).put(`${urlPrefix}role`,querystring.stringify(params))
    },
    deleteUser(token, params){
        console.log("service")
        console.log(querystring.stringify(params))
        return Api(token).delete(`${urlPrefix}user`, {data: querystring.stringify(params)})
    }
}