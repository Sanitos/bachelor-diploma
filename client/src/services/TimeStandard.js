import Api from './Api.js'
import querystring from 'querystring'
export default{
    getTimeStandard(token){
        return Api(token).get('/standard')
    },
    deleteTimeStandard(token, params){
        return Api(token).delete('/standard', {data: querystring.stringify(params)})
    },
    editTimeStandard(token, params){
        return Api(token).put('/standard', querystring.stringify(params))
    },
    addTimeStandard(token, params){
        return Api(token).post('/standard',querystring.stringify(params))
    }
}