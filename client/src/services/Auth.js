import Api from "../services/Api.js"
import querystring from 'querystring'
export default{
    getProfile(token){
        return Api(token).get('user')
    },
    login(params){
        return Api().post('user', params)
    },
    update(token, params){
        return Api(token).put('user', querystring.stringify(params))
    },
    delete_account(token){
        return Api(token).delete('user')
    },
    changePassword(token, params){
        return Api(token).put('password', querystring.stringify(params))
    }
}