import Api from './Api.js'
import querystring from 'querystring'
export default{
    get_approver(token){
        return Api(token).get('/approver')
    },
    set_approved(token, params){
        return Api(token).put('/approver', querystring.stringify(params))

    }
    // check_task(token, params){
    //     return Api(token).put('/verifier', qs.stringify(params))
    // }
}