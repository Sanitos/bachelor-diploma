import Api from './Api.js'
import qs from 'qs'
export default{
    get_plan(token){
        return Api(token).get('/verifier')
    },
    check_task(token, params){
        return Api(token).put('/verifier', qs.stringify(params))
    }
}