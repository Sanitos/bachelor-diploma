import Api from './Api.js'
import qs from 'qs'
export default{
    add(token, params){
        return Api(token).post('/individual-plan', qs.stringify(params))
    },
    get(token){
        return Api(token).get('/individual-plan')
    },
    delete_task(token, params){
        return Api(token).delete(`/individual-plan/${params}`)
    },
    update_task(token, params){
        return Api(token).put(`/individual-plan/${params.id}`, qs.stringify(params.body))
    },
    update_plan(token, params){
        return Api(token).put('/individual-plan', qs.stringify(params))
    },
    delete_plan(token){
        return Api(token).delete('/individual-plan')
    },
    update_table(token, params){
        return Api(token).put('/educational-table', qs.stringify(params))
    }
}