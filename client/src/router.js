import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Profile from './views/Profile.vue'
import Store from './store';
import TimeStanadard from './views/TimeStandard.vue'
import IndividualPlan from './views/IndividualPlan.vue'
import Wathcer from './views/Watcher.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/',
      name: 'home',
      beforeEnter: AuthGuard
    },
    {
      path: '/standard',
      name: 'timeStandard',
      component: TimeStanadard,
    },
    {
     path: '/individual-plan',
     name: 'individualPlan',
     component: IndividualPlan
    },
    {
      path: '/watcher',
      name: 'watcher',
      component: Wathcer
     },
     
  ] 
})
function AuthGuard(from, to, next) {
  if (Store.getters.getToken != null) {
    next();
  } else {
    next('/login');
  }
}