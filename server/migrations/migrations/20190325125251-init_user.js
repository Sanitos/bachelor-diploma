var bcrypt = require('bcrypt')
module.exports = {
  async up(db) {
    await db.createCollection('users')
    let pass =  await bcrypt.hash("admin", 10)
    let admin={
      email: "admin",
      password: pass,
      role: 'admin'
    }
    await db.collection('users').insertOne(admin)
  },

  async down(db) {
    await db.collection('users').drop()
  }
};
