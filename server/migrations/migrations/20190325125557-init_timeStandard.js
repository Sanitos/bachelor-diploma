const fs = require('fs')
module.exports = {
  async up(db) {
    await db.createCollection('timestandards')
    let data = fs.readFileSync('standard.json')
    let standardArr = JSON.parse(data)
    let inDB = []
    for (let cathedra in standardArr) {
      for (let section in standardArr[cathedra]) {
        for (let standard in standardArr[cathedra][section]) {
          standardArr[cathedra][section][standard]['section'] = section
          inDB.push(standardArr[cathedra][section][standard])
        }
      }
    }
    db.collection('timestandards').insertMany(inDB)
  },

  async down(db) {
    db.collection('timestandards').drop()
  }
};