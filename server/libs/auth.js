var jwt = require('jsonwebtoken')
var User = require("../models/user.js")

const checkToken = (req, res, next) => {
    const header = req.headers['authorization']
    if (typeof header !== 'undefined') {
        const auth_query = header.split(' ')
        const token = auth_query[1]
        req.body.token = token
        next()
    } else {
        res.send('access denied')
    }
}

const checkerType = async(token) =>{
    try {
        let decodeToken = await jwt.verify(token, process.env.KEY)
        let credentials = await User.findOne({
            _id: decodeToken.uid
        }).exec()
        if (credentials.role === "checker") {
            return credentials.subrole
        } else {
            return false
        }
    } catch (error) {
        return error
    }
}

const checkRole = async(token, role) =>{
    try {
        let decodeToken = await jwt.verify(token, process.env.KEY)
        let credentials = await User.findOne({
            _id: decodeToken.uid
        }).exec()
        if (credentials.role === role) {
            return true
        } else {
            return false
        }
    } catch (error) {
        return error
    }
}
const getCathedra = async(token) =>{
    try {
        let decodeToken = await jwt.verify(token, process.env.KEY)
        let credentials = await User.findOne({
            _id: decodeToken.uid
        }).exec()
        return credentials.cathedra
    } catch (error) {
        return error
    }
}

exports.checkToken = checkToken
exports.checkRole = checkRole
exports.checkerType = checkerType
exports.getCathedra = getCathedra



