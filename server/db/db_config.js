var mongoose = require('mongoose')
var dotenv = require('dotenv')
dotenv.config()
console.log(process.env.DATABASE_URL)
mongoose.connect(`${process.env.DATABASE_URL}/diploma`, {
    useNewUrlParser: true
})
var db = mongoose.connection;

db.on('error', function (error) {
  // If first connect fails because server-database isn't up yet, try again.
  // This is only needed for first connect, not for runtime reconnects.
  // See: https://github.com/Automattic/mongoose/issues/5169
  if (error.message && error.message.match(/failed to connect to server .* on first connect/)) {
    setTimeout(function () {
      mongoose.connect(`${DATABASE_URL}/diploma`, { useNewUrlParser: true }).catch((err) => {
        // empty catch avoids unhandled rejections
        if(err) console.log(err)
        console.log("connection failed");
      });
    }, 2 * 1000);
  } else {
    // Some other error occurred.  Log it.
    console.error(new Date(), String(error));
  }
});
db.once('open', () => {
    console.log('db success')
})
module.exports = db