var express = require("express")
var bcrypt = require('bcrypt')
var app = express()
var User = require("../models/user.js")
const Helper = require('../libs/auth.js')
var jwt = require('jsonwebtoken')
//create new user
app.post('/signup', Helper.checkToken, async (req, res) => {
    if (await Helper.checkRole(req.body.token, "admin")) {
        var user = new User({
            email: req.body.email,
            password: req.body.password,
            role: req.body.role,
            subrole: req.body.subrole,
            surname: req.body.surname,
            name: req.body.name,
            patronymic: req.body.patronymic,
            cathedra: req.body.cathedra
        })
        user.save((err, user) => {
            if (err) {
                if (err.code == 11000)
                    res.json({
                        err: "user already exist"
                    });
                else
                    res.json({
                        err: err
                    })

            } else {
                res.json({
                    uid: user.id
                })
            }
        })
    } else {
        res.json({
            err: 'not admin'
        })
    }

});
//list of all user
app.get('/userlist', Helper.checkToken, async (req, res) => {
    if (await Helper.checkRole(req.body.token, "admin")) {
        User.find((err, users) => {
            if (err) console.log(err)
            res.json(users)
        })
    } else {
        res.json({
            err: 'not admin'
        })
    }
});
//edit user role
app.put('/role', Helper.checkToken, async (req, res) => {
    if (await Helper.checkRole(req.body.token, "admin")) {
        let updated_obj = req.body
        console.log(updated_obj)
        delete updated_obj.token
        User.findOneAndUpdate({_id:req.body.uid},updated_obj,{new:true},(err, result)=>{
            if(err) console.log(err)
            res.json(result)
        })
    } else {
        res.json({
            err: 'not admin'
        })
    }
});
//delete user
app.delete('/user', Helper.checkToken, async (req, res)=>{
    if( await Helper.checkRole(req.body.token, "admin")){
        console.log(req.body)
        User.findOneAndDelete({_id: req.body.id}, (err, result)=>{
            if(err) console.log(err)
            res.json(result)
        })
    }
})

module.exports = app