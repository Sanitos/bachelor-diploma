var dotenv = require('dotenv')
dotenv.config()
var express = require('express')
var app = express()
var User = require("../models/user.js")
var bcrypt = require('bcrypt')
var jwt = require('jsonwebtoken')
var fs = require('fs');
const Helper = require('../libs/auth.js')
const Puppeteer = require('puppeteer')
const merge = require("easy-pdf-merge")
//login
app.post('/user', (req, res) => {
    User.findOne({
        email: req.body.email,
    }, (err, user) => {
        if (err) console.log(err)
        try {
            if (user != null) {
                bcrypt.compare(req.body.password, user.password, (err, result) => {
                    if (result === true) {
                        jwt.sign({
                            uid: user.id
                        }, process.env.KEY, (err, token) => {
                            if (err) console.log(err);
                            res.json({
                                token
                            })

                        })
                    } else {
                        res.json({
                            error: "403"
                        })
                    }
                })
            } else {
                res.json({
                    error: "403"
                })
            }
        } catch (error) {
            console.log(error)
        }
    })
});
//profile
app.get('/user/', Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            User.findOne({
                _id: userData.uid
            }).select('-password -__v').exec((err, user) => {
                if (err) console.log(err);
                res.json(user)
            })
        }
    })

});
//update profile
app.put('/user', Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            let query = {
                _id: userData.uid
            }
            let updated_profile = req.body
            delete updated_profile.token
            User.findOneAndUpdate(query, updated_profile, {
                new: true
            }, (err, user) => {
                if (err) console.log(err)
                else res.json(user)
            })
        }
    })

});

//delete user
app.delete('/user', Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            let query = {
                _id: userData.uid
            }
            User.deleteOne(query, (err, user) => {
                if (err) console.log(err)
                else res.json({
                    user
                })
            })
        }
    })

});
//generate pdf
app.post('/pdf', async (req, res) => {
    const browser_p = await Puppeteer.launch()
    const page_p = await browser_p.newPage()
    await page_p.setContent(req.body.html_portrait)
    await page_p.pdf({
        path: 'correct_portrait.pdf',
        format: 'A4',
    })
    browser_p.close()
    const browser_l = await Puppeteer.launch()
    const page_l = await browser_l.newPage()
    await page_l.setContent(req.body.html_landscape)
    await page_l.pdf({
        path: 'correct_landscape.pdf',
        format: 'A4',
        landscape: true
    })
    browser_l.close()
    merge(['correct_portrait.pdf', 'correct_landscape.pdf'], 'correct_full.pdf', (err) => {
        if (err) console.log(err)
        var fileName = 'correct_full.pdf'
        var data = fs.readFileSync(fileName);
        res.contentType("application/pdf");
        res.send(data);
        fs.unlink('correct_full.pdf', (err) => {
            if (err) console.log(err)
        })
        fs.unlink('correct_portrait.pdf', (err) => {
            if (err) console.log(err)
        })
        fs.unlink('correct_landscape.pdf', (err) => {
            if (err) console.log(err)
        })
    })
})
//change password
app.put("/password", Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            User.findOne({
                _id: userData.uid
            }, (err, result) => {
                bcrypt.compare(req.body.old_password, result.password, (err, result) => {
                    if (result === true) {
                        bcrypt.hash(req.body.new_password, 10, (err, hash) => {
                            if (err) {
                                console.log(err)
                            }
                            User.findOneAndUpdate({
                                _id: userData.uid
                            }, {
                                password: hash,
                                first_login: false
                            }, {
                                new: true
                            }, (err_update,result_update)=>{
                                if(err_update) console.log(err_update)
                                res.json(result_update)
                            })

                        })
                    } else {
                        res.json("incorrect password")
                    }
                })
            })
        }
    })
})

module.exports = app