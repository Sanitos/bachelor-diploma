var express = require('express')
var app = express()
var IndividualPlan = require('../models/individualPlan.js')
var Helper = require('../libs/auth.js')
var jwt = require('jsonwebtoken')
var TimeStandard = require('../models/timeStandard.js')
//get user individual-plan
app.get('/individual-plan/', Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            IndividualPlan.
            findOne({
                uid: userData.uid
            }).
            populate('tasks.timeStandard').
            exec((err, result) => {
                console.log(result);
                res.json(result)
            })
        }
    })

})
//add standard to individual-plan
app.post('/individual-plan', Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, async (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            var tasks = req.body.tasks
            for (task of tasks){
                let response = await TimeStandard.findOne({_id:task.timeStandard})
                task['scheduled_hours'] = response.time_standard*task.amount_of_work    
            } 
            IndividualPlan.findOneAndUpdate({
                uid: userData.uid
            }, {
                uid: userData.uid,
                $push: {
                    tasks: tasks
                }
            }, {
                upsert: true,
                new: true,
            }, (err, result) => {
                res.json(result)
            })
        }
    })

})
//update task in individual-plan 
app.put('/individual-plan/:task_id', Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, async (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            var query = {
                "tasks": {
                    $elemMatch: {
                        _id: req.params.task_id
                    }
                },
                uid: userData.uid
            }
            let response = await IndividualPlan.findOne(query).populate('tasks.timeStandard')
            let task = response.tasks.find((el)=>{
                return el._id = req.params.task_id
            });
            var scheduled_hours = task.timeStandard.time_standard*req.body.amount_of_work
            IndividualPlan.findOneAndUpdate(query, {
                $set: {
                    "tasks.$.amount_of_work": req.body.amount_of_work,
                    "tasks.$.worked_hours": req.body.worked_hours,
                    "tasks.$.scheduled_hours": scheduled_hours,
                    "tasks.$.proof_of": req.body.proof_of
                }
            }, {
                new: true
            }).
            populate('tasks.timeStandard').
            exec((err, result) => {
                    if (err) console.log(err);
                    res.json(result)
                }
            )
        }
    })
})
//delete task
app.delete('/individual-plan/:task_id', Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            IndividualPlan.findOneAndUpdate({
                uid: userData.uid
            }, {
                $pull: {
                    tasks: {
                        _id: req.params.task_id
                    }
                }
            }, {
                upsert: true,
                new: true
            }, (err, result) => {
                res.json(result)
            })

        }
    })
})
//delete plan
app.delete('/individual-plan', Helper.checkToken, (req, res) => {
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            IndividualPlan.findOneAndDelete({
                uid: userData.uid
            }, (err, result) => {
                if (err) console.log(err);
                res.json(result)
            })
        }
    })
})
//update plan
app.put('/individual-plan', Helper.checkToken, (req,res)=>{
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
            if (err) {
                res.json(err)
            } else {
                IndividualPlan.findOneAndUpdate({uid:userData.uid}, {on_check: req.body.on_check}, {new:true}, (err, result)=>{
                    if(err) console.log(err);
                    res.json(result)
                })
            }
        })
})
//route for educationalTable
app.put('/educational-table', Helper.checkToken, (req,res) =>{
    jwt.verify(req.body.token, process.env.KEY, (err, userData) => {
        if (err) {
            res.json(err)
        } else {
            IndividualPlan.findOne({uid:userData.uid}, (err, result)=>{
console.log(result)                
                function findTable(table) {
                    return table.educational_form == req.body.table.educational_form
                }
                let educationalWorkTable = result.educationalWorkTable
                let tableIndex = educationalWorkTable.findIndex(findTable)
                educationalWorkTable.splice(tableIndex,1,req.body.table)
                IndividualPlan.findOneAndUpdate({uid:userData.uid}, {educationalWorkTable: educationalWorkTable}, {new:true}, (err,result)=>{
                    if (err) console.log(err)
                    res.json(result)
                })
            })
        }
    })
})
module.exports = app