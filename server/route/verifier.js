var express = require('express')
//lodash
var _ = require('lodash')
var app = express()
var IndividualPlan = require("../models/individualPlan.js")
var Helper = require('../libs/auth.js')
app.get('/verifier', Helper.checkToken, async (req, res) => {
    var checkerType = await Helper.checkerType(req.body.token)
    var cathedra = await Helper.getCathedra(req.body.token)
    if (checkerType) {
        let response = await IndividualPlan.find({
            on_check: true
        }).
        populate({
            path: 'tasks.timeStandard',
            match: {
                section: checkerType,
            }
        }).
        populate({
            path: 'uid',
        })
        for (res_index in response) {
            Indexes = _.keys(_.pickBy(response[res_index].tasks, {
                timeStandard: null
            }))
            for (index in Indexes) {
                response[res_index].tasks.splice(response[res_index].tasks.findIndex(item => item.timeStandard === null), 1)
            }
            console.log(checkerType)
            if (response[res_index].tasks.length === 0 && checkerType != "educational") {
                delete response[res_index]
            }
        }
        let filteredArray = response.filter(el => el != null)
        console.log(filteredArray)
        res.json(filteredArray.filter((plan)=>{
            return plan.uid.cathedra == cathedra
        }))

    } else {
        res.json('not checker')
    }

})
app.put('/verifier', Helper.checkToken, async (req, res) => {
    console.log(req.body)
    var checkerType = await Helper.checkerType(req.body.token)
    if (checkerType) {
        var updateObj = {}
        if (req.body.checked !== undefined) {
            if (req.body.checked == "true") {
                updateObj = {
                    $addToSet: {
                        verifiers: checkerType
                    }
                }
            } else if (req.body.checked == "false") {
                updateObj = {
                    $pullAll: {
                        verifiers: checkerType
                    }
                }
            }
        }
        if (req.body.remark !== undefined) {
            updateObj.remark = req.body.remark
        }
        IndividualPlan.findOneAndUpdate({
            _id: req.body.id
        }, updateObj, {
            upsert: true,
            new: true
        }, (err, result) => {
            if (err) console.log(err);
            var verifiers_needed = ["educational", "scientific", "organizational", "educational-methodical", "educational-public"]
            var on_approved = _.isEqual(_.sortBy(verifiers_needed), _.sortBy(result.verifiers))
            var query = req.body.id
            IndividualPlan.findOneAndUpdate({
                _id: query
            }, {
                on_approved: on_approved,
            }, {
                new: true
            }, (err, result) => {
                if (err) console.log(err);
            })
            res.json(result)
        })
    } else {
        res.json('not checker')
    }

})
app.get('/approver', Helper.checkToken, async (req, res) => {
    var cathedra = await Helper.getCathedra(req.body.token)
    let response = await IndividualPlan.find({
        on_approved: true
    }).
    populate({
        path: 'tasks.timeStandard',
    }).
    populate({
        path: 'uid',
    })
    res.json(response.filter((plan)=>{
        return plan.uid.cathedra == cathedra
    }))
    // IndividualPlan.find({on_approved: true}, (err, result)=>{
    //     if(err) console.log(err);
    //     res.json(result)
    // })
})
app.put('/approver', Helper.checkToken, async (req, res) => {
    IndividualPlan.findOneAndUpdate({
        _id: req.body.id
    }, req.body, {
        new: true
    }, (err, result) => {
        if (err) console.log(err);
        console.log(req.body)
        res.json(result)
    })
})
module.exports = app