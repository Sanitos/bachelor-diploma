var express = require('express')
var app = express()
var TimeStandard = require('../models/timeStandard.js')
var Helper = require("../libs/auth.js")
//get collection 
app.get('/standard/:type', Helper.checkToken, (req, res) => {
    TimeStandard.find({
        section: req.params.type
    }, (err, result) => {
        res.json(result)
    })
})
//get collections
app.get('/standard', Helper.checkToken, async (req, res) => {
    if (await Helper.checkRole(req.body.token, "admin")) {
        console.log('admin')
        TimeStandard.find((err, result) => {
            if (err) console.log(err);
            res.json(result)
        })
    } else {
        console.log('not admin')
        TimeStandard.find(async (err, result) => {
            if (err) console.log(err);
            var cathedra = await Helper.getCathedra(req.body.token)
            res.json(result.filter((standard) => {
                console.log(cathedra)
                return standard.cathedra == cathedra
            }))
        })
    }

})
//add standard to collection
app.post('/standard', Helper.checkToken, async (req, res) => {
    if (await Helper.checkRole(req.body.token, "admin")) {
        var standard = new TimeStandard({
            section: req.body.section,
            work_type_title: req.body.work_type_title,
            unit: req.body.unit,
            time_standard: req.body.time_standard
        })
        standard.save((err, result) => {
            if (err) console.log(err)
            res.json(result)
        })
    } else {
        res.json({
            err: "not admin"
        })
    }
})
//delete standard
app.delete('/standard', Helper.checkToken, async (req, res) => {
    if (await Helper.checkRole(req.body.token, "admin")) {
        let query = {
            _id: req.body.id
        }
        TimeStandard.deleteOne(query, (err, result) => {
            if (err) console.log(err);
            res.json(result)
        })
    } else {
        res.json({
            err: 'not admin'
        })
    }
})
//update standard
app.put('/standard', Helper.checkToken, async (req, res) => {
    if (await Helper.checkRole(req.body.token, "admin")) {
        let query = {
            _id: req.body._id
        }
        let obj = {
            section: req.body.section,
            work_type_title: req.body.work_type_title,
            unit: req.body.unit,
            time_standard: req.body.time_standard
        }
        TimeStandard.findOneAndUpdate(query, obj, {
            new: true
        }, (err, result) => {
            if (err) console.log(err);
            res.json(result)
        })
    }
})
module.exports = app