const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const cors = require('cors')
require('./db/db_config.js')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());

app.get('/', (req, res) => {
    res.json("server api");
});

app.use('/api', require("./route/auth.js"))
app.use('/api/admin', require("./route/admin.js"))
app.use('/api', require("./route/timeStandard.js"))
app.use('/api', require("./route/individualPlan.js"))
app.use('/api', require("./route/verifier.js"))
var port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log('Example app listening on port ' + port);
}); 