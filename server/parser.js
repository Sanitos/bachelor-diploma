const excelToJson = require('convert-excel-to-json');
const fs = require('fs')
const result = excelToJson({
    sourceFile: './standard.xlsx',
    columnToKey: {
        A: 'work_type_title',
        B: 'unit',
        C: 'time_standard'
    }
});
for(section in result){
    for (standard in result[section]){
        result[section][standard]['cathedra'] = process.argv[2]
    }
}
var json  = JSON.stringify(result)
fs.writeFile(`standard-${process.argv[2]}.json`, json, 'utf8', (err)=>{
    if(err) console.log(err)
    else console.log('ok')
})