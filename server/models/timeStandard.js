var mongoose = require('mongoose')
var timeStandardSchema = new mongoose.Schema({
    section:{
        type: String,
        required: true
    },
    work_type_title:{
        type: String,
        required: true,
    },
    unit:{
        type: String,
        required: true,
    },
    time_standard:{
        type: Number,
        required: true,
    },
    cathedra:{
        type: String,
        required: true
    }
})
var TimeStandard = mongoose.model('TimeStandard', timeStandardSchema)

module.exports = TimeStandard