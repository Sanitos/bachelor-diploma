const mongoose = require('mongoose')
var Schema = mongoose.Schema
var TaskSchema = Schema({
    timeStandard: {
        type: Schema.Types.ObjectId,
        ref: 'TimeStandard'
    },
    amount_of_work: {
        type: Number,
        required: true
    },
    scheduled_hours: {
        type: Number,
    },
    worked_hours: {
        type: Number,
        default: 0
    },
    proof_of: {
        type: String,
        default: null
    },
    
})
var individualPlanSchema = Schema({
    uid: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    tasks: [TaskSchema],
    on_check: {
        type: Boolean,
        default: false
    },
    verifiers: [String],
    approved: {
        type: Boolean,
        default: false
    },
    on_approved: {
        type: Boolean,
        default: false
    },
    educationalWorkTable: {
        type: Array,
        default: [{
            educational_form: "fullTime",
            table: []
        },
        {
            educational_form: "notFullTime",
            table: []
        }]
    },
    remark: {
        type: String,
    }
})
var IndividualPlan = mongoose.model('IndividualPlan', individualPlanSchema)
module.exports = IndividualPlan