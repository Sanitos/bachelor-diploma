const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    surname:{
        type: String,
        default: "Surname"
    },
    name:{
        type: String,
        default: "name"
    },
    patronymic:{
        type: String,
        default: "patronymic"
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    subrole:{
        type: Array,
        default: []
    },
    cathedra:{
        type: String,
        defaul: 'none',
        required: true
    },
    first_login:{
        type: Boolean,
        default: true
    }
})

UserSchema.pre('save', function (next) {
    var user = this;
    bcrypt.hash(user.password, 10, function (err, hash) {
        if (err) {
            return next(err);
        }
        user.password = hash;
        next();
    })
});
var User = mongoose.model('User', UserSchema)




module.exports = User