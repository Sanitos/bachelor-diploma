# MEVN Stack app for PoltNTU
You can launch it with **Docker**
>1. git clone https://gitlab.com/Sanitos/bachelor-diploma.git 
>2. cd *bachelor-diploma*
>3. docker-compose build
>4. docker-compose up 

DEMO - https://bachelor-diploma.herokuapp.com